import Vue from 'vue'
import App from './App'

import { JjimWidget } from './js_sdk/jiguang_jim.js'

import char from './pages/char/index.vue'
Vue.component('char',char)

import mail from './pages/mail/index.vue'
Vue.component('mail',mail)

import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)


Vue.config.productionTip = false

Vue.use(new JjimWidget());

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()

 



